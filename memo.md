# Notes

Pour générer le package docusaurus

```bash
docker run --rm -v "$(pwd):/data" node:16 npx @docusaurus/init@latest init ovh-formation classic
sudo chown ${UID} ovh-formation
```

Pour lancer le site :

```bash
docker run --rm -v "$(pwd):/data" -v "$(pwd)/.gitconfig:/root/.gitconfig" -w /data/ -p 3000:3000 -p 35729:35729 --name docusaurus -e NPM_CONFIG_LOGLEVEL=verbose --network host node:lts npx docusaurus start
```

## Images

Illustration by [Viktoriya Belinio]("https://icons8.com/illustrations/author/XTPoH093lluQ")
