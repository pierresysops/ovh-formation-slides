---
sidebar_position: 3
---

# Stockage objet

> Rédigé le 3 mars 2024

Dans cette partie, nous allons créer notre premier bucket et jouer avec des objets.

## Création du premier bucket via le manager

Dans le manager, dans la partie Public Cloud, nous avons plusieurs types de stockage possibles. Nous cliquons sur `Object Storage` :

Comme pour chaque début de projet, la page propose des liens vers des tutoriels qui peuvent être utiles à lire (cela peut donner des idées d'usages comme par exemple le `Disaster Recovery`).

![Page Object Storage](../img/ovh-pc-object-storage.png)

Nous créons notre premier conteneur d'objets (bucket) en cliquant sur le bouton `Créer un conteneur d'objets`.

Nous pouvons choisir le type de stockage que nous souhaitons :

- standard
- high performance
- swift

Le dernier, `swift`, est le mode de stockage d'OpenStack. Nous ne l'utiliserons pas dans ce chapitre.

Le type de stockage va influer sur le prix. La grille des tarifs se trouve [ici](https://www.ovhcloud.com/fr/public-cloud/prices/#storage).

Pour nos exercices, nous prendrons la classe `standard`.

L'étape suivante consiste à choisir dans quel centre de données vous souhaitez héberger vos fichiers.

Dans notre exemple, nous choisissons Roubaix.

Pour pouvoir lire et/ou écrire dans un bucket, il faut un utilisateur. Pour ce premier exercice, nous créons un utilisateur depuis cette page. Nous verrons par la suite que nous pouvons préparer les utilisateurs à l'avance.

![Création du bucket](../img/ovh-pc-object-storage-create-bucket-1.png)

Nous donnons une description à notre utilisateur. Cela pourrait être le nom que nous donnerons à notre bucket. Cela peut être plus pratique par la suite pour s'y retrouver dans les usages.

![Création utilisateur S3](../img/ovh-pc-object-storage-create-user-1.png)

Cela vous affichera les informations d'identification pour cet utilisateur. Il vous faut les conserver dans un emplacement sûr (un gestionnaire de mots de passe comme Bitwarden, VaultRS, Proton Pass, PassBolt par exemple).

![Identifiants de l'utilisateur](../img/ovh-pc-object-storage-create-user-identifiants.png)

Nous donnons le nom de notre bucket et nous aurons terminé pour la création de notre bucket.

![Nom du bucket et création](../img/ovh-pc-object-storage-create-bucket-2.png)

On va vouloir tester dès à présent si on peut pousser un objet dans notre bucket.

### Ajouter et supprimer un objet dans notre premier bucket

L'idée n'est pas de refaire un énième tutoriel sur comment installer sous Linux l'outil `awscli` et comment le configurer. Nous pouvons reprendre le [tutoriel d'OVHcloud](https://help.ovhcloud.com/csm/en-ie-public-cloud-storage-s3-getting-started-object-storage?id=kb_article_view&sysparm_article=KB0047339)

```bash
pip3 install awscli awscli-plugin-endpoint
```

### Configuration awscli (sous Linux)

Dans votre `HOME_DIR`, nous allons créer le dossier `.aws` s'il n'est pas déjà présent :

```bash
mkdir $HOME/.aws
```

Nous avons besoin des fichiers suivants :

- config :

```bash
vi $HOME/.aws/config
#Placer les informations suivantes :
[plugins]
endpoint = awscli_plugin_endpoint

[default]
#la région est rbx dans notre exemple pour Roubaix, sbg pour Strasbourg, gra pour Gravelines...
region = rbx
s3 =
 #le endpoint est donné en cliquant sur le bucket depuis le manager (rbx = Roubaix; sbg = Strasbourg...)
 endpoint_url = https://s3.rbx.io.cloud.ovh.net 
 signature_version = s3v4
 max_concurrent_requests = 100
 max_queue_size = 1000
 multipart_threshold = 50MB
 # Edit the multipart_chunksize value according to the file sizes that you want to upload. The present configuration allows uploading files up to 10 GB (100 requests * 10MB). For example, setting it to 5GB allows you to upload files up to 5TB.
 multipart_chunksize = 10MB
s3api =
 endpoint_url = https://s3.rbx.io.cloud.ovh.net
output = json
```

- credentials :

```bash
vi $HOME/.aws/credentials
#Placer les informations suivantes
[default]
aws_access_key_id = accessidrecupéréesurlemanager 
aws_secret_access_key = secretaccesrecupéréesurlemanager
```

Il est préférable de mettre les droits les plus petits sur ce fichier.

```bash
chmod 400 $HOME/.aws/credentials
```

Si vous avez besoin de récupérer les identifiants, vous pouvez vous rendre dans `votre projet` --> `Object Storage` --> `Utilisateur S3`

![Identifiants S3](../img/ovh-pc-object-storage-get-credentials.png)

Nous pouvons vérifier à présent l'accès au bucket :

```bash
aws s3 ls
```

Vous devriez voir votre bucket. En cas d'erreur, vérifiez dans un premier temps les identifiants dans votre fichier `credentials` et la configuration du fichier `config`.

Par défaut, l'utilisateur a tous les droits sur tous les buckets du projet. Nous verrons plus tard comment limiter ces droits.

### Ajout d'un objet

Nous allons envoyer un fichier sur notre bucket. Pour cela, créons un fichier sinon vous pouvez prendre un document en votre possession :

```bash
cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-4096} | head -n ${2:-1} > /tmp/fichier.txt
```

Envoyons ce fichier dans notre bucket qui a pour nom `formations3` dans notre exemple :

```bash
aws s3api put-object --bucket formations3 --key fichier.txt --body /tmp/fichier.txt
#retour de la commande
{
    "ETag": "\"f7aad75482347bb682b4b6ff2c9838c9\"",
    "VersionId": "1709482696.531147"
}
```

Listons les objets de notre bucket :

```bash
aws s3api list-objects --bucket formations3
#retour de la commande
{
    "Contents": [
        {
            "Key": "fichier.txt",
            "LastModified": "2024-03-03T16:18:16.000Z",
            "ETag": "\"f7aad75482347bb682b4b6ff2c9838c9\"",
            "Size": 4097,
            "StorageClass": "STANDARD",
            "Owner": {
                "DisplayName": "suitedecaractères:user-suitedecaractèrequelonretrouvedanslemanager",
                "ID": "suitedecaractères:user-suitedecaractèrequelonretrouvedanslemanager"
            }
        }
    ],
    "RequestCharged": null
}
```

L'objet est bien sûr visible depuis le manager.

![Premier objet dans le bucket](../img/ovh-pc-object-storage-bucket-first-object.png)

### Suppression d'un objet

Nous supprimons maintenant notre objet :

```bash
aws s3api delete-object --bucket formations3 --key fichier.txt
#retour de la commande
{
    "VersionId": "1709482696.531147"
}
```

Vérifions que l'objet n'est plus disponible :

```bash
aws s3api list-objects --bucket formations3
#retour de la commande
{
    "RequestCharged": null
}
```

On peut vérifier depuis le manager qu'il n'y a plus d'objet :

![Bucket vide](../img/ovh-pc-object-storage-bucket-empty.png)

## Objet en privé et téléchargement public

Lors de la création du bucket, nous avions décidé de le mettre en `privé`. On peut vérifier que l'objet `fichier.txt` n'est pas téléchargeable. Le lien serait : URL_du_bucket/nomdelobjet soit dans notre cas `https://formations3.s3.rbx.io.cloud.ovh.net/fichier.txt`

Cependant, pour chaque objet, nous pouvons générer un lien de téléchargement. Il aura une durée limitée. (Le guide  [ici https://docs.aws.amazon.com/AmazonS3/latest/userguide/ShareObjectPreSignedURL.html](https://docs.aws.amazon.com/AmazonS3/latest/userguide/ShareObjectPreSignedURL.html)) :

```bash
aws s3 presign s3://formations3/fichier.txt --expires-in 604800
#retour de la commande
https://s3.rbx.io.cloud.ovh.net/formations3/fichier.txt?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=8f126999b6cd453287f68136541b3e12%2F20240303%2Frbx%2Fs3%2Faws4_request&X-Amz-Date=20240303T163655Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=2643541bf94ef2659679052835acaa09bf303745c0e7d4619c15359a5794cdd5
```

Vous pouvez tester immédiatement après la commande la validité du lien.

## Guides OVH

Les fonctionnalités S3 implémentées par OVHcloud sont indiquées [ici https://help.ovhcloud.com/csm/en-ie-public-cloud-storage-s3-compliancy?id=kb_article_view&sysparm_article=KB0047469](https://help.ovhcloud.com/csm/en-ie-public-cloud-storage-s3-compliancy?id=kb_article_view&sysparm_article=KB0047469).

Il est important de prendre connaissance des limites techniques [ici https://help.ovhcloud.com/csm/fr-public-cloud-storage-s3-limitations?id=kb_article_view&sysparm_article=KB0047372](https://help.ovhcloud.com/csm/fr-public-cloud-storage-s3-limitations?id=kb_article_view&sysparm_article=KB0047372).
