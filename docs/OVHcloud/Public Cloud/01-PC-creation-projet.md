---
sidebar_position: 1

---

# Création d'un projet

> Rédigé le aout 2023

Nous créons notre premier projet Public Cloud.

Tout d'abord, vous trouverez sur le site d'OVHcloud une présentation des offres/services proposées par l'hébergeur :

![Présentation Public Cloud par OVH](https://www.ovhcloud.com/fr/public-cloud/)

On y retrouve (liste non exhaustive):

- la création d'instances à la demande avec paiement à l'heure ou au mois
- du Kubernetes managé
- une registry Docker
- du Container as a Service
- des bases de données managées
- du stockage objet compatible S3 et Swift
- de l'IP failover

## Notre premier projet

Depuis mon manager OVHcloud, rendez-vous dans la partie [Public Cloud](https://www.ovh.com/manager/#/public-cloud/pci/projects/onboarding) :

![Création projet Public Cloud](../img/ovh-pc-creation-projet-1ere-etape.png)

Nous cliquons sur le bouton `Créer votre premier projet OVH Public Cloud`.

Il vous faut accepter les contrats et donner un nom à votre projet. Dans le cadre de cet exercice, nous le nommerons `SYSOPSFORM`.

![Formulaire création projet Public Cloud](../img/ovh-pc-creation-projet-2d-etape.png)

Il vous faut ensuite ajouter un moyen de paiement. Comme il est précisé, si on ne consomme rien, il n'y a pas de prélèvement.
Les moyens les plus rapides pour faire des tests sont la carte bancaire et PayPal. Nous vous laissons le choix pour la suite.

Dès que vous avez passé les étapes de paiement, la page de création du projet s'affiche. Il faut entre 1 et 3 minutes environ.

Il arrive très régulièrement qu'OVHcloud offre des crédits pour la création d'un projet Public Cloud. N'hésitez pas à voir la page de présentation plus haut.

## Petit tour du projet

Nous sommes dans la page de notre projet :

![Notre projet Public Cloud](../img/ovh-pc-creation-projet-accueil.png)

Sur le menu de gauche, nous avons toutes les ressources que l'on peut créer et sur la partie centrale les ressources créées.

Pour le moment, nous n'avons encore rien.

Si vous avez un code fourni par OVHcloud pour des crédits, cela s'active dans le menu de gauche sur `Crédits et bons`.

Avant de créer nos premières ressources, nous devons faire un peu d'administratif/gestion.

### Clés SSH

Premièrement, nous ajoutons **nos clés SSH**. Pour cela, dans le menu de gauche, cliquez sur `Clés SSH`, puis sur `+ Ajouter une clé SSH`.

![Menu ajout clés SSH](../img/ovh-pc-ajout-sshkey-1ere-etape.png)

Si vous avez une clé SSH, copiez-collez son contenu dans la fenêtre et donnez-lui un nom.

Dans le cas contraire, ouvrez un terminal sur votre poste et tapez la commande suivante :

```bash
ssh-keygen -b 521 -t ECDSA
#il est conseillé de protéger par mot de passe votre clés
```

Dans un environnement Linux, la paire de clés se trouvera dans le dossier `~/.ssh/`. Il vous faut renseigner sur la page d'OVH la partie publique :

```bash
cat ~/.ssh/id_ecdsa.pub
```

![Ajout de la clés SSH](../img/ovh-pc-ajout-sshkey-2d-etape.png)

La clés apparaît sur la page web.

### Quota et Régions

Dans cette partie, vous visualisez les ressources consommées par région (nous reviendrons sur ce terme dans une autre partie) et la quantité maximale autorisée. Si vous avez besoin de quantité supplémentaire (espace disque, CPU, RAM...), vous devez ouvrir un ticket au support.

Lorsque le projet est créé, vous avez accès à un grand nombre de régions mais pas à l'ensemble, par exemple, vous aurez accès à GRA7 et GRA11 mais pas GRA9. Depuis cette page, vous pouvez demander l'accès aux autres régions.

### Contacts and Rights

Dans la partie `gestion des utilisateurs`, nous avions vu la possibilité d'ajouter des utilisateurs et de leur affecter des rôles. Ici, on peut autoriser des utilisateurs OVHcloud à accéder à ce projet spécifiquement soit en lecture soit en lecture/écriture. Cela peut être utile si l'on souhaite donner accès au projet à des collaborateurs. Un exemple de cas d'usage est un projet OVH par équipe produit de l'entreprise.

Vous pouvez aussi définir ici le contact facturation. Dans un contexte entreprise, votre collaborateur chargé des récupérer et traiter les factures peut se créer un compte OVHcloud. Il vous fournira son nickhandle et vous pourrez ainsi le définir comme `contact facturation`. Ainsi, votre collègue recevra chaque mois automatiquement par mail la facture du projet. Il est possible de définir le contact de facturation dans la partie `Mon compte -> Mes contacts -> Mes services`.

### Billing Control

Cette page nous permet de contrôler l'état des dépenses. Il est possible de définir un seuil à partir duquel un mail d'alerte est envoyé au gestionnaire du compte OVH. Ainsi, il est possible d'affecter des crédits à chaque projet.

Dans l'onglet `Estimation de ma prochaine facture`, vous pouvez `créer une alerte` :

![Créer une alerte facture](../img/ovh-pc-gestion-projet-alerte-facture.png)

Dans cette alerte, nous indiquons l'adresse mail à prévenir et à partir de quel seuil la déclencher. Il est important de noter que cela n'arrête pas la consommation des ressources. Sinon cela serait problématique si arbitrairement OVH se mettait à supprimer des ressources.

![Définition seuil d'alerte facturation](../img/ovh-pc-gestion-projet-alerte-facture-seuil.png)

### Project settings

On peut renommer notre projet dans cette partie ou le supprimer.

Il est aussi possible de demander à bénéficier de l'offre Hébergement de Données de Santé (HDS). Cependant, cette option n'est possible qu'en souscrivant au support de niveau Business ou Entreprise.

### Users and Roles

Nous viendrons créer des utilisateurs dans cette partie lorsque nous manipulerons la console Horizon, le stockage objet S3 et Terraform.

En quelques mots, nous pouvons créer des utilisateurs techniques et leur affecter des rôles. Il ne s'agit pas de comptes permettant d'accéder au manager d'OVH.

Ces utilisateurs pourront manipuler les API d'Openstack pour
