---
sidebar_position: 2
---

# Création d'instances

> Rédigé août 2023

Création de notre première instance depuis le manager OVHcloud

## Les grands principes

Avant de créer notre première instance, examinons rapidement quelques notions :

- les régions
- les types d'instances
- le stockage
- les règles de pare-feu

### Régions

Vous pouvez créer des instances dans de nombreux centres de données d'OVH répartis dans le monde (Europe, Asie ou Amérique). Pour chaque centre de données, il y a un découpage par numéro.
Par exemple pour le centre de données de Gravelines en France, il y a GRA3, GRA5, GRA7... Il s'agit de zones physiquement différentes sur le même lieu géographique. Pour autant, il faut prendre en compte que ces régions au sein d'un même centre peuvent être très proches physiquement.

Dans une stratégie de sauvegarde ou de pérennité des données, on veillera toujours à mettre de la distance entre les données exploitées et les sauvegardes. L'avantage étant de pouvoir résister à des problèmes du type :

- dégât physique sur les équipements (inondation, incendie, tremblement de terre...)
- coupure physique des fibres optiques
- pannes générales d'électricité
- corruption de données sur les disques

Cependant, cette stratégie présente l'inconvénient de restaurer des données plus lentement car les données sont physiquement éloignées. Nous reviendrons dans une autre partie sur les stratégies de sauvegardes que l'on peut déployer.

### Modèles

OVH, comme les autres hébergeurs, propose des modèles d'instances avec des nombres de vCPU et de RAM prédéfinis. Il y a des groupes de modèles :

- General Purpose : Les instances à usage général offrent un équilibre entre RAM et performances.
- CPU : Les instances de calcul optimisé sont idéales pour les applications nécessitant des fréquences de calcul élevées ou de la parallélisation de tâches.
- RAM : Les instances à mémoire optimisée sont recommandées pour vos bases de données, analyses et calculs en mémoire, ainsi que d'autres applications gourmandes en RAM.
- GPU : Les instances de calcul accéléré (GPU, FPGA) sont jusqu'à 1 000 fois plus rapides qu'un CPU sur certaines applications (rendu, transcodage vidéo, bio-informatique, Big Data, deep learning, etc.)
- Discovery : Les instances à ressources partagées (Discovery) sont adaptées aux tests, recettes et environnements de développement. Leurs performances peuvent légèrement varier au fil du temps.
- IOPS : Les instances IOPS fournissent les transactions disque les plus rapides de la gamme Public Cloud.
- Metal : Les instances metal proposent des serveurs physiques à la demande, livrés en quelques minutes et facturés à l'heure ou au mois.

Il est possible de passer d'un modèle à un autre sous certaines conditions :

- prendre le modèle en formule flexible
- passer vers un modèle dont la partition racine est de taille équivalente ou supérieure à celle actuelle
- utiliser une partition racine dédiée

Nous étudierons chacun de ces cas de figure pour comprendre les enjeux, les avantages et les inconvénients.

Une instance est créée dans une région. Elle est visible uniquement dans celle-ci. Il est possible de copier une instance vers une autre région.

### Stockage

OVH propose deux types de stockages :

- mode bloc (bloc storage)
- mode objet (object storage)

Le mode bloc est le plus connu, il s'agit d'espace disque alloué qui sera vu comme un disque physique par votre instance. Il peut être réduit ou agrandi. On peut faire un parallèle avec les disques gérés par un hyperviseur pour des machines virtuelles. Un espace disque est alloué par l'hyperviseur à une VM et cette dernière voit cet espace comme un disque physique. Depuis l'hyperviseur, on peut agrandir ou réduire cet espace.

Il est possible d'ajouter plusieurs espaces de stockage bloc à une instance. Il est aussi possible de partager des espaces de stockage entre instances au sein d'une même région.

**Un espace de stockage bloc ne peut excéder 4 To**. Via LVM2, on peut sur une instance agréger plusieurs volumes et ainsi dépasser les 4 To, mais ce ne serait pas la solution la plus adaptée. OVH propose des [services de stockages](https://www.ovhcloud.com/fr/storage-solutions/) permettant de dépasser cette capacité.

Un espace de stockage est visible et manipulable uniquement dans sa région. Il est possible d'exporter cet espace pour l'importer dans une autre région.

Le stockage objet gère les données sous forme d'objets individuels que l'on appelle via des API. On ne voit pas les objets en local. Il faut les manipuler via des outils ou via du code. Une définition du stockage objet pourrait être :

> Le stockage objet est un système de stockage de données dans lequel les données sont organisées sous forme d'objets. Chaque objet est associé à un identifiant unique (parfois appelé une clé) qui permet d'y accéder. Chaque objet peut contenir à la fois les données elles-mêmes et des métadonnées descriptives.

Dès lors que l'on souhaite développer une application orientée cloud (conteneur, Kubernetes), il est primordial, pour ne pas dire indispensable, que l'application utilise le stockage objet.

## Création de l'instance

Nous allons créer notre première instance.

Depuis le manager, dans `Public Cloud` --> `Votre projet` --> `Instances`, cliquez sur `Créer une instance` :

![Créer une instance](../img/ovh-pc-creation-instance-1ere-etape.png)

Nous sélectionnons un modèle, pour réduire les coûts, prenons le plus petit : `Discovery D2-2`

![Choix du modèle](../img/ovh-pc-creation-instance-modele.png)

Choisissez la région de votre choix :

![Choix de la région](../img/ovh-pc-creation-instance-region.png)

OVHcloud vous propose différentes distributions Linux, Windows et même quelques applications pré-installées comme Docker ou cPanel par exemple. Nous partirons dans cet exercice avec une version Ubuntu LTS. Il est nécessaire de sélectionner votre clé SSH. Si vous ne l'avez pas ajoutée dans la partie `Création du projet`, vous pouvez le faire ici en cliquant sur `Ajouter une clé`.

Il est à noter que le choix de l'OS Windows entraîne un coût supplémentaire au prix de l'instance.

À l'étape suivante, plusieurs options s'offrent à vous :

- vous pouvez d'un coup créer plusieurs instances en une fois. Cela peut être intéressant si vous avez besoin d'avoir 4 machines Ubuntu Server de type B2-15 dans la même région. Vous allez donner un nom à l'instance (champ suivant). OVH ajoutera un numéro incrémenté à la suite de ce nom.
- vous pouvez donner un nom à votre instance. Dans notre exercice, nous la nommerons `application-web`. Ce nom sera aussi le nom de la machine lorsque vous ferez `hostnamectl` une fois connecté en SSH.
- Vous pouvez ajouter un script post-installation. Nous allons faire un exemple simple :

```bash
#!/bin/bash

echo 'automatiquement réalisé' > /root/auto.txt
```

- **Sauvegarde automatique :** Chaque nuit, l'instance sauvegarde uniquement la partition racine de l'instance. Cette pratique est particulièrement intéressante dans un contexte de production. Cependant, il est important de considérer vos besoins, notamment si vos machines peuvent être créées et supprimées à la volée sans soucis de pérennité des données.

![Nom de notre instance et la quantité](../img/ovh-pc-creation-instance-nom.png)

Nous arrivons à l'étape du réseau. Vos instances peuvent être accessibles sur Internet en direct, ou elles peuvent être créées au sein d'un réseau privé (VLAN). Pour que l'instance puisse être exposée sur Internet et/ou accéder à Internet, il faudra mettre en place d'autres services (que nous verrons dans d'autres parties).

Pour notre première instance, nous allons l'exposer sur Internet.

![Choix du réseau](../img/ovh-pc-creation-instance-reseau.png)

Nous pouvons payer à l'heure notre instance ou au mois. Il est avantageux de prendre au mois dans les conditions suivantes :

- nous savons que l'instance est nécessaire pour une durée supérieure à 15 jours
- que nous sommes en début du mois (avant le 15 du mois)

Dans notre cas, notre instance aura une durée de vie de quelques minutes.

![Choix de la facturation](../img/ovh-pc-creation-instance-facturation.png)

L'instance est en cours de création.

![Création de l'instance](../img/ovh-pc-creation-instance-encours.png)

Après quelques secondes, l'instance est prête :

![Instance créée](../img/ovh-pc-creation-instance-cree.png)

En cliquant sur l'instance, vous accédez aux détails :

![Détails de l'instance](../img/ovh-pc-creation-instance-details.png)

Nous tentons de nous connecter en SSH. L'utilisateur par défaut est `ubuntu`. Si nous avions pris une distribution Debian, l'utilisateur par défaut aurait été `debian` et ainsi de suite.

```bash
ssh ubuntu@57.128.65.103
sudo ls -l /root/
#on voit le fichier auto.txt
sudo cat /root/auto.txt
```

On constate que le script post-installation s'est correctement éxecuté.

Nous supprimons l'instance :

![Suppression instance](../img/ovh-pc-creation-instance-suppression.png)

Il est à noté qu'éteindre une instance facturée à l'heure coutera uniquement le prix du stockage de l'espace disque de la partition racine. En revanche, si la machine est facturée au mois, il n'y aura pas d'incidence sur la facturation, l'instance restera facturée au prix indiqué.
