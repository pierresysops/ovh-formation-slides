---
sidebar_position: 1
---

# Présentation d'OVHcloud

> Rédigé le 2 mars 2024

L'idée dans cette partie est de présenter très succinctement l'hébergeur et les arguments permettant de le choisir pour héberger ses données en production (sans ordre d'importance) :

* centres de données en France sur le territoire métropolitain
* société immatriculée en France
* société sans lien avec les Patriot Act et Cloud Act
* **prédictibilité** des factures
* pas de coût de trafic pour les instances public cloud et les serveurs physiques
* pare-feu inclus sur les instances public cloud et les serveurs physiques
* impliqué en tant que fournisseur de logiciels libres et/ou open source
* des centres de données propres à OVHcloud dans différents [pays de l'Union Européenne et du monde](https://www.ovhcloud.com/fr/datacenters-ovhcloud/)
* une démarche RSE
* une communication, que je juge sincère, lors d'un incident à l'inverse d'autres hébergeurs pour des incidents similaires
* plusieurs niveaux de souscriptions au support (gratuit, coupe-file sur le gratuit, business et sur mesure)
* à partir d'un certain niveau de budget dépensé (rien d'officiel, expérience personnelle > 50K€/an), un account manager dédié
* des nouveaux produits/services qui sortent régulièrement
* des offres à tarifs compétitifs
* de nombreuses API pour gérer ses instances, DNS, comptes, IAM, stockage S3...
* un Discord pour échanger avec une communauté en français et anglais et avec des salariés d'OVHcloud (sans qu'ils fassent support)
* des certifications obtenues et en cours HDS, SecNumCloud, ISO 27001/27017/27018/27701/5001 qui peuvent être parfois nécessaires pour remporter des marchés
* prône le `no-vendor-locking`

[Information récupérée sur le site d'OVHcloud](https://www.ovhcloud.com/fr/about-us/) :

> Nous fournissons des solutions de cloud public et privé, d'hébergement mutualisé et de serveurs dédiés dans 140 pays à travers le globe. Nous proposons également à nos clientes et clients l'enregistrement de noms de domaine, de la téléphonie, ainsi que de l'accès à Internet. Créé en 1999, OVHcloud est une entreprise française présente dans le monde entier, grâce à la localisation internationale de ses datacenters et points de présence.

[L'histoire en quelques mots](https://corporate.ovhcloud.com/fr/company/history/)

[Les certifications](https://corporate.ovhcloud.com/fr/trusted-cloud/security-certifications/)

Enfin, laissons OVHcloud présenter ses arguments :

> [Un écosystème ouvert et adapté](https://corporate.ovhcloud.com/fr/company/why-ovhcloud/)

J'ajouterai :

Le `no-vendor-locking` est un élément important dans le choix d'un hébergeur. J'ai eu à changer plusieurs fois d'hébergeur ou à accompagner des clients dans ce type de projets. Ce n'est jamais simple. L'idée principale à mon sens est la possibilité de pouvoir extraire ses données facilement et de manière autonome.
Cependant, il y a un aspect moins visible dans le `no-vendor-locking` qui n'est pas souvent affiché. On se crée soit-même le `vendor-locking` en se spécialisant dans le fonctionnement (paramétrage, configuration, création de machines, gestion...) d'un hébergeur. Les hébergeurs tels que Scaleway, AWS, GCP, Azure, AlibabaCloud... proposent des services similaires (création d'instance à la demande, serveur physique, DNS, stockage objet...). La gestion de ces ressources diffère totalement entre eux. Et bien que l'on puisse migrer et récupérer plus ou moins facilement ses données, le changement d'un hébergeur sera surtout compliqué par la nécessité d'apprendre le fonctionnement d'un autre hébergeur.
En ce sens, OVHcloud a toujours affiché vouloir travailler aussi sur cet aspect-là. Ils n'hésitent pas à dire qu'il ne faut pas choisir un seul hébergeur mais au contraire faire du multi-cloud. En cela, je trouve cette ouverture de la part d'OVHcloud un argument en leur faveur.
