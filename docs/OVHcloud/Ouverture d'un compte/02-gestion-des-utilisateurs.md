---
sidebar_position: 2
---

# Gestion des utilisateurs

> Rédigé août 2023

D'autres utilisateurs peuvent gérer le compte OVHcloud. Chaque personne possède sa propre identité, il n'y a pas de partage de compte.
On peut aussi définir des rôles par utilisateur.

Dans la page `Mon compte`, cliquez sur l'onglet `Gestion des utilisateurs`.

Vous pouvez configurer une connexion à votre gestionnaire d'identité, comme Keycloak par exemple. Dans notre cas, nous allons créer localement un utilisateur qui aura les droits d'administration.

Cliquez sur `Ajouter un utilisateur` :

![Ajout d'un utilisateur](../img/ovhcloud-ajout-utilisateur-1ere-etape.png)

Un formulaire apparaît pour que vous puissiez renseigner les informations de l'utilisateur (identifiant, adresse mail, mot de passe et groupe/rôle).

![Formulaire ajout d'un utilisateur](../img/ovhcloud-ajout-utilisateur-formulaire.png)

Dès que vous validez le formulaire, l'utilisateur est créé et peut se connecter.

Dans un autre navigateur ou en navigation privée, rendez-vous sur le site d'[OVHcloud](https://ovh.com) et connectez-vous avec ce nouveau compte.
Une fois connecté, vous êtes invité à mettre en place la double authentification. Cependant, vous n'y êtes pas contraint. C'est l'inconvénient. Il n'y a pas de moyen de contrôle depuis le compte principal pour obliger les utilisateurs à mettre en place la 2FA.

En revanche, la restriction par IP publique est activée. Les utilisateurs ne pourront pas se connecter s'ils n'arrivent pas depuis une des IP autorisées depuis le compte principal.
