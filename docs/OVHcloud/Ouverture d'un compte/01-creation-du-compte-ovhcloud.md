---
sidebar_position: 1
---

# Création d'un compte chez OVHcloud

> Rédigé août 2023

* Ouvrez un navigateur pour vous rendre sur le site d'[OVHcloud](https://www.ovhcloud.com/fr/)
* Cliquez sur `Mon compte client`
* Renseignez vos informations personnelles dans la partie `Je n'ai pas encore de compte OVHcloud`

![Formulaire création d'un compte](../img/ovhcloud-creation-compte-formulaire.png)

Vous allez recevoir un code sur l'adresse mail renseignée. Il faudra l'indiquer sur la page dédiée.

À partir de là, on accepte les conditions générales et l'on renseigne le nouveau formulaire.

![Formulaire de type de compte](../img/ovhcloud-creation-compte-type-compte.png)

Votre compte est créé !

En haut à droite, vous voyez votre nom, le type de support souscrit (pour le moment `Support standard`) et le plus important, **votre `nickhandle`**.

## Sécurisation du compte

Dès la création du compte, vous êtes invité à mettre en place la double authentification.

Nous allons réaliser cela et plus encore pour sécuriser l'accès au manager d'OVHcloud.

Pour cela, cliquez sur votre nom en haut à droite :

![Accès aux détails de notre compte](../img/ovhcloud-acces-details-du-compte.png)

Vous arrivez sur la page de votre compte. Nous nous rendons dans l'onglet `Sécurité`.

![Détails du compte, onglet sécurité](../img/ovhcloud-details-compte-onglet-securite.png)

Comme on peut le voir, il est possible de :

* changer son mot de passe
* gérer la double authentification
* restreindre l'accès au manager depuis des IP publiques

Sur ce dernier point, je conseille vivement de mettre en place la restriction sur des IP publiques (cela sera utile si vous créez d'autres utilisateurs, partie Gestion des utilisateurs). Nous partons de l'hypothèse que vous avez une ou des IPs fixes.

### Restriction d'accès par IP

Si vous avez une seule IP fixe, il est préférable, et confirmé par un conseiller chez OVHcloud, d'en trouver une seconde. Cela peut être celle du domicile, celle d'un petit serveur qui vous servira de relais. Dans un contexte professionnel, vous aurez peut-être plusieurs IP de plusieurs FAI. Dans ce contexte, c'est l'idéal.

Il faut ajouter une IP avec la règle `autorisée`.

![Ajout d'une IP autorisée à accéder au manager](../img/ovhcloud-acces-par-ip-autorisee.png)

Par la suite, vous pouvez mettre la règle par défaut à `refusé`. Ainsi, il ne sera plus possible de se connecter autrement que depuis votre réseau interne (société, chez vous, VPN). **On veillera à cocher la case nous avertissant en cas de tentative d'accès.**

![Interdire l'accès au manager depuis une IP inconnue par défaut](../img/ovhcloud-acces-refuse-par-defaut.png)

### Double authentification par SMS

Sur l'onglet sécurité du compte, nous cliquons sur `Activer la double authentification`.

Comme vous avez dû créer un compte et vous avez dû renseigner votre numéro de téléphone, nous pouvons supposer ici que vous recevez des SMS. Nous activons donc en premier lieu la double authentification par SMS. Cependant, dans un contexte professionnel avec un niveau de criticité fort, la sécurité par SMS est décriée pour les raisons de duplication de carte SIM. En sécurité, il s'agit surtout de savoir placer un curseur par rapport au niveau d'attaque que nous pouvons être amenés à subir et que nous pouvons être en capacité de gérer.

Dans le but de l'exercice, nous ajouterons donc l'authentification par SMS.

![Ajout authentification par SMS](../img/ovhcloud-authentification-sms-1ere-etape.png)

Confirmation du code reçu par SMS :

![Confirmation du code SMS](../img/ovhcloud-authentification-sms-2d-etape.png)

Comme il s'agit du premier système de double authentification mis en place, une fenêtre avec des codes de secours apparaît.

Il est important de copier les codes de secours dans un lieu sécurisé. Cela peut être sur :

* une clé USB chiffrée dans le coffre-fort (banque, entreprise)
* un fichier chiffré via VeraCrypt ou clés GPG et déposé sur un espace sécurisé
* imprimé sur un papier et placé dans un coffre-fort (banque, entreprise)
* gestionnaire de mot de passe (différent de celui dans lequel vous aurez stocké le mot de passe d'accès à votre compte OVHcloud)

Si vous ajoutez plusieurs numéros de téléphone, il est important de savoir que tous les numéros recevront un code (différent) au moment de l'authentification par SMS.

### Double authentification par OTP

Nous partons de l'hypothèse que vous avez un smartphone ou un gestionnaire de mot de passe intégrant la fonctionnalité OTP.

Si vous êtes sous Android, je conseille l'application [AEGIS](https://getaegis.app/). Elle a les avantages :

* d'être open source, repo [GitHub](https://github.com/beemdevelopment/Aegis)
* de permettre la sauvegarde chiffrée de vos clés OTP
* de pouvoir rechercher parmi toutes vos clés
* de verrouiller/déverrouiller l'accès à vos via un mot de passe ou la biométrie

Nous restons dans l'onglet `Sécurité` et dans la partie `Double authentification`.

Nous cliquons sur `Ajouter une application`.

![Ajout authentification par OTP](../img/ovhcloud-authentification-otp-1ere-etape.png)

Cela vous ouvre un popup avec un QRCode à scanner via votre application d'OTP.
L'application va vous donner un code à 6 chiffres. Vous devez l'indiquer sur la page web pour valider l'ajout de cette méthode d'authentification. Vous pouvez donner un descriptif à cette application. Cela peut être utile si vous ajoutez plusieurs téléphones/applications d'OTP. Par exemple, si vous avez un téléphone principal et un de secours.

Si votre application d'OTP, comme AEGIS, vous permet d'exporter et/ou sauvegarder vos secrets OTP, il faut le faire.

### Double authentification par clés de sécurité

Dans cette partie, vous devez disposer d'une clé physique compatible avec U2F comme les Yubikey 5 par exemple.

Nous cliquons sur `Ajouter une clé` :

![Ajout d'une clé](../img/ovhcloud-authentification-sms-1ere-etape.png)

Vous n'avez plus qu'à brancher votre clé et lui donner un nom sur la page web d'OVHcloud. Cela est important si vous êtes plusieurs personnes au sein de l'équipe IT à devoir/pouvoir vous authentifier sur le compte OVHcloud.
