---
sidebar_position: 1
---

# Envoyer une nouvelle version d'un TFSTATE sur Gitlab

> Rédigé le 31 mars 2024

## Contexte

Nous utilisons Gitlab en `backend` pour gérer le `tfstate` de notre code Terraform.

## Besoin

Nous avons besoin de changer le `tfstate` présent sur Gitlab. Nous pourrions supprimer la version actuelle pour revenir à une itération précédente mais partons du principe que cela ne suffirait pas.
Notre problématique est telle qu'il est nécessaire de modifier à la main le contenu du fichier `json` et de le renvoyer sur Gitlab.

## Réponse

Je n'ai pas trouvé d'API sur Gitlab permettant d'envoyer une nouvelle version d'un `tfstate` sur Gitlab mais on peut le faire via la commande `terraform state push`.

Depuis notre projet sur Gitlab, nous récupérons notre `tfstate`, dans le menu `Operate` -> `Terraform states`

![Gitlab menu tfstate](./img/Gitlab_menu_tfstate.png)

On enregistre notre `tfstate` sur notre PC (dans /tmp par exemple), avec le nom `projet.json`

![Téléchargement du tfstate](./img/Gitlab_telechargement_tfstate.png)

Vous pouvez éditer le fichier pour modifier le contenu selon votre convenance/besoin, **en incrémentant le champ `serial`** :

![Incrémente le serial](./img/tfstate_serial.png)

Pour pousser le nouveau fichier, vous pouvez partir de votre projet terraform utilisant le backend Gitlab ou créer un projet temporaire. Pour l'exemple, nous utiliserons un dossier temporaire.

Dans votre terminal :

```bash
mkdir /tmp/projet
cd /tmp/projet
touch backend.tf
# Ajouter la ligne suivante dans ce fichier
terraform { backend "http" {} }
```

Nous avons besoin de nous créer un `token` personnel pour pousser sur Gitlab, dans son `Profile` -> `Edit Profile`

![Menu edit profile](./img/Gitlab_profile_menu.png)

Dans `Acces token`, ajoutez un `token` avec une date d'expiration proche dans le temps :

![New token](./img/Gitlab_new_token.png)

Copiez le `token` dans une variable dans un terminal :

```bash
export GITLAB_ACCESS_TOKEN='letokengenerealinstant'
```

Dans la page où vous avez téléchargé votre `tfstate`, vous pouvez afficher la commande `terraform init`

![Gitlab terraform init](./img/Gitlab_command_terraform_init.png)

Copiez le code affiché dans votre terminal, sauf la partie `GITLAB_ACCESS_TOKEN`

![Terraform init command](./img/Gitlab_command_terraform_init_2.png)

A partir de là, nous pouvons pousser notre fichier `tfstate` :

```bash
terraform state push -lock=true  /tmp/projet.json 
```

Nous pouvons vérifier la bonne prise en compte de la nouvelle version du `tfstate` en téléchargeant à nouveau le `tfstate` depuis Gitlab.

## Les petites découvertes

Pour récupérer la version d'un tfstate via les API :

`curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<your_project_id>/terraform/state/<your_state_name>/versions/<version-serial>"`

Si vous avez accès au serveur de votre Gitlab, voici comment retrouver l'emplacement de votre fichier `tfstate` (pris sur la documentation de Gitlab)

>
>Terraform state files are stored in the hashed directory path of the relevant project.
>
>The format of the path is `/var/opt/gitlab/gitlab-rails/shared/terraform_state/<path>/<to>/<projectHashDirectory>/<UUID>/0.tfstate`, where UUID is randomly defined.
>
>To find a state file path:
>
>1.Add `get-terraform-path` to your shell:
>
>```bash
>get-terraform-path() {
>  PROJECT_HASH=$(echo -n $1 | openssl dgst -sha256 | sed 's/^.* //')
>  echo "${PROJECT_HASH:0:2}/${PROJECT_HASH:2:2}/${PROJECT_HASH}"
>}
>```
>
> 2.Run `get-terraform-path <project_id>`.
>
> ```bash
> $ get-terraform-path 650
> 20/99/2099a9b5f777e242d1f9e19d27e232cc71e2fa7964fc988a319fce5671ca7f73
> ```
>
> The relative path is displayed.

Le fichier est normalement chiffré.

## Sources

- [https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#manage-individual-terraform-state-versions](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#manage-individual-terraform-state-versions)
- [https://hackernoon.com/fr/un-guide-pratique-pour-migrer-l%27%C3%A9tat-de-terraform-vers-gitlab-cicd](https://hackernoon.com/fr/un-guide-pratique-pour-migrer-l%27%C3%A9tat-de-terraform-vers-gitlab-cicd)
- [https://docs.gitlab.com/ee/administration/terraform_state.html](https://docs.gitlab.com/ee/administration/terraform_state.html)
