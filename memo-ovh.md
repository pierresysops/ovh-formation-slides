# Formation / trasnfert de compétences sur OVHcloud

Ce projet à pour but de partager des connaissances sur certains services de l'hébergeur OVHcloud.

Tous les services proposés par l'hébergeur ne pourront être abordés.

Les pages seront complétées au fure et à mesure du temps. Il s'agit d'une initiative personnelle, il n'y a aucun lien avec OVHcloud.

Pour un même service plusieurs usages peuvent en être réalisés, il ne sera donc pas possible d'être exhaustif.

Au delà d'une présentation d'un service, par exemple le lancement d'une instance Public Cloud, nous verrons comment déployer avec l'outil Terraform.

Dans les premières grandes lignes le sommaire pourrait être celui-ci :

- Présentation d'OHVcloud
- Création d'un compte :
  - Sécuriser son compte
  - Gestion des utilisateurs

- Public Cloud :

  - Création d'un projet
  - Ma première instance
  - Stockage objet - une vue rapide
  - Accès à Horizon
  - Utilisation d'un vRACK
  - Gestion des volumes
  - Image personnalisée
  - Gateway
  - LoadBalancer
  - anti-affinité

- DNS

  - Acheter son domaine
  - Ses premières entrées
  - Gestion avec Terraform

- BareMetal :
  - Acquisition de son premier serveur
  - Pare-feu

- API OVHcloud

  - Petit tour d'horizon
  - Première utilistion avec Python

- OpenStack

  - Rapide prise en main avec Terraform
  - Une proposition de gestion des instances
  - Stockage objet

- S3

  - Gérer les accès
  - Gestion avec Terraform

- IAM

  - Présentation rapide
  - Utilisation
